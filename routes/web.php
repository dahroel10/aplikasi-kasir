<?php

use App\Http\Controllers\{
    DBController,
    KategoriController,
    LaporanController,
    ProdukController,
    MembersController,
    PengeluaransController,
    PembeliansController,
    PembeliansDetailController,
    PenjualansController,
    PenjualansDetailController,
    SettingController,
    SupplierController,
    UserController,
};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [DBController::class, 'index'])->name('dashboard');

    Route::group(['middleware' => 'level:1'], function () {
        Route::get('/kategori/data', [KategoriController::class, 'data'])->name('kategori.data');
        Route::resource('/kategori', KategoriController::class);

        Route::get('/produk/data', [ProdukController::class, 'data'])->name('produk.data');
        Route::post('/produk/delete-selected', [ProdukController::class, 'deleteSelected'])->name('produk.delete_selected');
        Route::post('/produk/cetak-barcode', [ProdukController::class, 'cetakBarcode'])->name('produk.cetak_barcode');
        Route::resource('/produk', ProdukController::class);

        Route::get('/member/data', [MembersController::class, 'data'])->name('member.data');
        Route::post('/member/cetak-member', [MembersController::class, 'cetakMember'])->name('member.cetak_member');
        Route::resource('/member', MembersController::class);

        Route::get('/supplier/data', [SupplierController::class, 'data'])->name('supplier.data');
        Route::resource('/supplier', SupplierController::class);

        Route::get('/pengeluaran/data', [PengeluaransController::class, 'data'])->name('pengeluaran.data');
        Route::resource('/pengeluaran', PengeluaransController::class);

        Route::get('/pembelian/data', [PembeliansController::class, 'data'])->name('pembelian.data');
        Route::get('/pembelian/{id}/create', [PembeliansController::class, 'create'])->name('pembelian.create');
        Route::resource('/pembelian', PembeliansController::class)
            ->except('create');

        Route::get('/pembelian_detail/{id}/data', [PembeliansDetailController::class, 'data'])->name('pembelian_detail.data');
        Route::get('/pembelian_detail/loadform/{diskon}/{total}', [PembeliansDetailController::class, 'loadForm'])->name('pembelian_detail.load_form');
        Route::resource('/pembelian_detail', PembeliansDetailController::class)
            ->except('create', 'show', 'edit');

        Route::get('/penjualan/data', [PenjualansController::class, 'data'])->name('penjualan.data');
        Route::get('/penjualan', [PenjualansController::class, 'index'])->name('penjualan.index');
        Route::get('/penjualan/{id}', [PenjualansController::class, 'show'])->name('penjualan.show');
        Route::delete('/penjualan/{id}', [PenjualansController::class, 'destroy'])->name('penjualan.destroy');
    });

    Route::group(['middleware' => 'level:1,2'], function () {
        Route::get('/transaksi/baru', [PenjualansController::class, 'create'])->name('transaksi.baru');
        Route::post('/transaksi/simpan', [PenjualansController::class, 'store'])->name('transaksi.simpan');
        Route::get('/transaksi/selesai', [PenjualansController::class, 'selesai'])->name('transaksi.selesai');
        Route::get('/transaksi/nota-kecil', [PenjualansController::class, 'notaKecil'])->name('transaksi.nota_kecil');
        Route::get('/transaksi/nota-besar', [PenjualansController::class, 'notaBesar'])->name('transaksi.nota_besar');

        Route::get('/transaksi/{id}/data', [PenjualansDetailController::class, 'data'])->name('transaksi.data');
        Route::get('/transaksi/loadform/{diskon}/{total}/{diterima}', [PenjualansDetailController::class, 'loadForm'])->name('transaksi.load_form');
        Route::resource('/transaksi', PenjualansDetailController::class)
            ->except('create', 'show', 'edit');
    });

    Route::group(['middleware' => 'level:1'], function () {
        Route::get('/laporan', [LaporanController::class, 'index'])->name('laporans.indexs');
        Route::get('/laporan/data/{awal}/{akhir}', [LaporanController::class, 'data'])->name('laporans.data');
        Route::get('/laporan/pdf/{awal}/{akhir}', [LaporanController::class, 'exportPDF'])->name('laporans.export_pdf');

        Route::get('/user/data', [UserController::class, 'data'])->name('user.data');
        Route::resource('/user', UserController::class);

        Route::get('/setting', [SettingController::class, 'index'])->name('settings.index');
        Route::get('/setting/first', [SettingController::class, 'show'])->name('setting.show');
        Route::post('/setting', [SettingController::class, 'update'])->name('setting.update');
    });
 
    Route::group(['middleware' => 'level:1,2'], function () {
        Route::get('/profil', [UserController::class, 'profil'])->name('user.profil');
        Route::post('/profil', [UserController::class, 'updateProfil'])->name('user.update_profil');
    });
});