<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Seeder;

class ProdukTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produk::insert([
            [
                'id_kategori' => '4',
                'nama_produk' => 'Sabun Batang',
                'kode_produk' => 'P000001',
                'merk' => 'Lifebuoy',
                'harga_beli' => '1500',
                'diskon' => '0',
                'harga_jual' => '3000',
                'stok' => '100'
            ],
            [
                'id_kategori' => '2',
                'nama_produk' => 'Mie Instant',
                'kode_produk' => 'P000002',
                'merk' => 'Sedap',
                'harga_beli' => '1000',
                'diskon' => '0',
                'harga_jual' => '1500',
                'stok' => '100'
            ],
            [
                'id_kategori' => '1',
                'nama_produk' => 'Pensil',
                'kode_produk' => 'P000003',
                'merk' => 'Joyco',
                'harga_beli' => '500',
                'diskon' => '0',
                'harga_jual' => '1000',
                'stok' => '100'
            ],
            [
                'id_kategori' => '3',
                'nama_produk' => 'Kopi Sachet',
                'kode_produk' => 'P000004',
                'merk' => 'ABC',
                'harga_beli' => '500',
                'diskon' => '0',
                'harga_jual' => '1500',
                'stok' => '100'
            ],
            [
                'id_kategori' => '3',
                'nama_produk' => 'Air Minum Galon',
                'kode_produk' => 'P000005',
                'merk' => 'Le Minerale',
                'harga_beli' => '10000',
                'diskon' => '0',
                'harga_jual' => '20000',
                'stok' => '100'
            ]
        ]);
    }
}
