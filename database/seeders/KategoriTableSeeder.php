<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kategori::insert([
            [
                'nama_kategoris' => 'ATK'
            ],
            [
                'nama_kategoris' => 'Makanan'
            ],
            [
                'nama_kategoris' => 'Minuman'
            ],
            [
                'nama_kategoris' => 'Alat Mandi'
            ]
        ]);
    }
}
